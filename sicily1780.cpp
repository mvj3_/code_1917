//题目网址: 
//http://soj.me/1780

//题目分析:
//其实这题只是变相的进制转换。。。7进制
//不过从0 1 2 3 4 5 6变成0 1 3 4 6 7 9
//只要算出7进制下表示的数，然后对应数字对换一下就好
//例如25在标准7进制表示为34，34对应分别为46，46即为新进制下的表示。
//可以自己写写看看。
//所以思路就变得很简单了
//先转换成标准7进制，然后用个表对应转换一下。
//最后输出答案，可能要算下幂次和。技巧在代码处，时间很快的。

#include <iostream>
#include <string>

using namespace std;

int miles[10] = {0, 1, -1, 2, 3, -1, 4, 5, -1, 6};

int main()
{
    int t, result;
    string str;
    int ans[9] = {0};
    cin >> t;
    while (t--) {
        result = 0;
        cin >> str;

        for (int i = 0; i < str.length(); i++) { ans[i] = miles[str[i]-'0']; }

        int seven = 1;
        for (int j = str.length() - 1; j >= 0; j--) {
            result += ans[j] * seven;
            seven *= 7;
        }

        cout << str << ": " << result << endl;
    }
    
    return 0;
}                                 